#!/usr/bin/env python3

def run(tool_arg):
    if '-bz' in tool_arg:
        from tool_func import bz
        bz.run()