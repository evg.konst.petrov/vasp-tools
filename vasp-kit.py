#!/usr/bin/env python3
import sys
import os
kit_dir = os.path.dirname(os.path.realpath(__file__))
sys.dont_write_bytecode = True
kit_mode = None
print('Welcome to VASP.kit! Have fun.')

# list of possible TOOL options.
possible_tool_args = ['-bz']
possible_read_args = ['-b', '--bands', '-d', '--dos']

from read_func.jobs import possible_tool_args, possible_read_args
# if one of TOOL-options set, switch to TOOL mode
# switch to READ mode otherwise
if len(sys.argv) > 1:
    for arg in sys.argv[1:2]:
        if arg in possible_tool_args:
            kit_mode = 'tool'
            print(f'Switching to TOOL mode: {arg}')
            import vasp_tool
            vasp_tool.run(arg)
        else:
            print(f'Switching to READ mode: {arg}')
            kit_mode = 'read'
            import vasp_read
            vasp_read.run(arg)
# switch to reading mode if no option set
else:
    print('Switching to READ mode.')
    kit_mode = 'read'
    import vasp_read

sys.exit()