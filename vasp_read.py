#!/usr/bin/env python3
import sys
import os

def run(read_arg):
    global job, key_args, out_dir, out_dirname, cwd, file_args, target_filename
    # get some arguments, paths, etc
    cwd = os.getcwd()

    key_args = []   # var for keys like '-efermi=0.123'
    file_args = []
    # check if there any keys set and divide them into key_args and dir_args
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            if arg[0] == '-':
                key_args.append(arg)
            else:
                file_args.append(arg)
    
    if read_arg in ['-b','--bands']:
        job = 'bands'
    elif read_arg in ['-d','--dos']:
        job = 'dos'
    elif read_arg in ['-phb', '--phonon_bands']:
        job = 'phbands'
    else:
        job = ''


    # from read_jobs.read_input import inp, structure, reciprocal
    
    out_dirname = f'_data_{job}'
    try:
        from read_jobs.read_input import inp
        out_dirname += '_' + inp.name
    except:
        if len(file_args) >= 1:
            target_filename = file_args[0]
            out_dirname += '_' + file_args[0].split('.')[0]
            if len(file_args) > 1:
                print('There is more than one file args. Using only the 1st one...')
        else:
            print('Failed detect the name of your setup in order to create a new directory.')
            print('Maybe something wrong with you OUTCAR / POSCAR / INCAR.')
            print('However, in the case of PHONON jobs you can IGNORE this warning.')
        
    out_dir = cwd + f'/{out_dirname}'
    try:
        os.mkdir(out_dir)
    except:
        pass
    
    # call proper script according to job value
    if job == 'dos':
        print(f'Reading density of states...')
        from read_jobs import read_dos
        read_dos.run()

    elif job == 'bands':
        print('Reading band structure \n')
        from read_jobs import read_bands
        read_bands.run()

    elif job == 'wavefunction':
        print(
            f'Reading wave function using WaveTrans...')
        from read_jobs import read_wf
        read_wf.run()

    elif job == 'phbands':
        print('Reading phonon band structure.')
        from read_jobs import read_phonon_bands
        read_phonon_bands.run()
        
    print('Done.')
# sys.exit()
