#!/usr/bin/env python3
import os
import numpy as np
from read_func.func import *

def run():
    if True:    
        regular_bands()

def regular_bands():
    from read_func.bands_func import Bands
    from vasp_read import out_dir, cwd, key_args
    from read_jobs.read_input import reciprocal, structure, inp

    bands = Bands()

    # go to proper dir
    os.chdir(out_dir)

    # save all this
    print('Saving bandstructure... ')
    E_file = bands.out_names['bands']
    kpath_file = bands.out_names['kpath']
    kpoints_file = bands.out_names['kpoints']
    np.savez_compressed(E_file, bands.bands)
    np.savez_compressed(kpath_file, reciprocal.kpath)
    np.savez_compressed(kpoints_file, reciprocal.kpoints)

    O_file = bands.out_names['orb']
    S_file = bands.out_names['spin']
    if '--no-procar' not in key_args and bands.if_sphproj:
        O, S = bands.orb_and_spin
        print('Saving O_file... ')
        np.savez_compressed(O_file, O)
        print('Saving S_file... ')
        np.savez_compressed(S_file, S)
        
    print('Saving addtional info...')
    # write additional python script with useful info for drawing
    info_dict = {
            'name': structure.name, 
            'Nb': inp.Nb,
            'Ne': inp.Ne,
            'Nk': reciprocal.Nk,
            'nspin': inp.nspin,
            'efermi': inp.get_efermi(),
            'N_at': structure.N_at,
            'at_list': structure.at_list,
            'at_types': structure.at_types,
            'at_nums': structure.at_numbers,
            'knames': reciprocal.knames,
            'kindex': reciprocal.kindex,
            'orb_list': bands.orb_list,
            'spin_axes_list': inp.spin_axes_list,
            'E_file': E_file + '.npz',
            'O_file': O_file + '.npz',
            'S_file': S_file + '.npz',
            'kpath_file': kpath_file + '.npz',
            'kpoints_file': kpoints_file + '.npz',
            'at_pos_dir': 'np.array(\n' + np.array2string(\
                structure.at_pos_dir, separator=', ') + '\n)',
            'at_pos_cart': 'np.array(\n' + np.array2string(\
                structure.at_pos_cart, separator=', ') + '\n)'
            }

    write_info( info_dict )

    templates_list = [('bands_E.py', '_E.py')]

    if bands.if_sphproj:
        if inp.nspin == 3:
            for i, axis in enumerate(bands.spin_axes_list):
                templates_list.append( ('bands_S.py', f'_S{axis}.py' ) )
        elif inp.nspin == 1:
            templates_list.append( ( 'bands_S.py', '_Sz.py') )

        for i, orb in enumerate(bands.orb_list):
            templates_list.append( (f'bands_O.py', f'_O_{orb}.py') )
        if inp.LORBIT in [11, 12]:
            templates_list.append( (f'bands_O.py', f'_O_p.py') )
            templates_list.append( (f'bands_O.py', f'_O_d.py') )
            if any(['f' in orb for orb in bands.orb_list]):
                templates_list.append( (f'bands_O.py', f'_O_f.py') )

        if inp.LORBIT in [11, 12]:
            templates_list.append( (f'bands_O_overall.py', f'_O_overall.py') )
        elif inp.LORBIT == 10:
            templates_list.append( (f'bands_O_overall_lorbit=10.py', f'_O_overall.py') )

    bands.save_templates( templates_list )
    if bands.if_sphproj:
        for i, axis in enumerate(bands.spin_axes_list):
            with open(f'_S{axis}.py', 'r') as f:
                file = f.read()
            with open(f'_S{axis}.py', 'w') as f:
                file = file.replace('<axis_name>', str(axis))\
                    .replace('<axis_num>', str(i))
                f.write(file)
        
        for i, orb in enumerate(bands.orb_list):
            with open(f'_O_{orb}.py', 'r') as f:
                file = f.read()
            with open(f'_O_{orb}.py', 'w') as f:
                file = file.replace('<orb_num>', f'{i}')\
                    .replace('<orb_name>', orb)
                f.write(file)
        if inp.LORBIT in [11, 12]:
            sum_orbs = [ [1,2,3], [4,5,6,7,8] ]
            sum_orbs_names = ['p','d','f']
            if any(['f' in orb for orb in bands.orb_list]):
                sum_orbs.append([ 9,10,11,12,13,14,15 ])
            for i_orbs, orb in zip(sum_orbs, sum_orbs_names):
                with open(f'_O_{orb}.py', 'r') as f:
                    file = f.read()
                with open(f'_O_{orb}.py', 'w') as f:
                    tmp = ', '.join(map(str, i_orbs))
                    file = file.replace('<orb_num>', tmp)\
                        .replace('<orb_name>', orb)
                    f.write(file)


    # delete massive variables
    del(bands)

