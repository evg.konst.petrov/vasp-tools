#!/usr/bin/env python3
import os
import numpy as np
from read_func.func import *

def run():
    if True:    
        regular_phonon_bands()

def regular_phonon_bands():
    from read_func.phonopy_func import Phonopy_bands
    from vasp_read import out_dir, cwd, key_args, target_filename, out_dirname

    phbands = Phonopy_bands(target_filename)

    # go to proper dir
    os.chdir(out_dir)

    # save all this
    print('Saving bandstructure... ')
    E_file = phbands.out_names['bands']
    kpath_file = phbands.out_names['qpath']
    kpoints_file = phbands.out_names['qpoints']
    np.savez_compressed(E_file, phbands.get_ph_bands() )
    np.savez_compressed(kpath_file, phbands.get_qpath() )
    np.savez_compressed(kpoints_file, phbands.get_qpoints() )
        
    print('Saving addtional info...')
    # write additional python script with useful info for drawing
    info_dict = {
            'name': out_dirname, 
            'Nq': phbands.Nq,
            'N_at': phbands.N_at,
            # 'at_list': structure.at_list,
            # 'at_types': structure.at_types,
            # 'at_nums': structure.at_numbers,
            'qnames': [], #reciprocal.knames,
            'qindex': phbands.get_spec_q(),
            'E_file': E_file + '.npz',
            'qpath_file': kpath_file + '.npz',
            'qpoints_file': kpoints_file + '.npz',
            # 'at_pos_dir': 'np.array(\n' + np.array2string(\
            #     structure.at_pos_dir, separator=', ') + '\n)',
            # 'at_pos_cart': 'np.array(\n' + np.array2string(\
            #     structure.at_pos_cart, separator=', ') + '\n)'
            }

    write_info( info_dict )

    templates_list = [('phbands_E.py', '_phE.py')]

    phbands.save_templates( templates_list )

    # delete massive variables
    del(phbands)

