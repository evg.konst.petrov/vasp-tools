#!/usr/bin/env python3
import os
import numpy as np
from read_func.func import *

def run():
    if True:
        regular_dos()

def regular_dos():
    from read_func.dos_func import Dos
    from vasp_read import out_dir, cwd, key_args
    from read_jobs.read_input import reciprocal, structure, inp
    
    dos = Dos()

    # go to proper dir
    os.chdir(out_dir)

    # save all this
    print('Saving DOS... ')
    egrid_file = dos.out_names['egrid']
    dos_file = dos.out_names['dos']
    dos_lms_file = dos.out_names['dos_orb']
    dos_slms_file = dos.out_names['dos_slms']
    np.savez_compressed(egrid_file, dos.egrid)
    np.savez_compressed(dos_file, dos.dos)

    if dos.if_sphproj:
        np.savez_compressed(dos_lms_file, dos.dos_orb)
        np.savez_compressed(dos_slms_file, dos.dos_slms)
        
    print('Saving addtional info...')
    # write additional python script with useful info for drawing
    info_dict = {
            'name': structure.name, 
            'Nb': inp.Nb,
            'Ne': inp.Ne,
            'nspin': inp.nspin,
            'efermi': inp.get_efermi(),
            'N_at': structure.N_at,
            'at_list': structure.at_list,
            'at_types': structure.at_types,
            'at_nums': structure.at_numbers,
            'orb_list': dos.orb_list,
            'spin_axes_list': inp.spin_axes_list,
            'egrid_file': egrid_file + '.npz',
            'emin': np.min(dos.egrid),
            'emax': np.max(dos.egrid),
            'DOS_tot_file': dos_file + '.npz',
            'DOS_lms_file': dos_lms_file + '.npz',
            'DOS_slms_file': dos_slms_file + '.npz',
            'at_pos_dir': 'np.array(\n' + np.array2string(\
                structure.at_pos_dir, separator=', ') + '\n)',
            'at_pos_cart': 'np.array(\n' + np.array2string(\
                structure.at_pos_cart, separator=', ') + '\n)'
            }

    write_info( info_dict )

    templates_dict = {
        '_DOS_tot.py': '_DOS_tot.py'
    }

    # if bands.if_sphproj:
    #     if inp.nspin == 1:
    #         templates_dict.update(
    #             {'bands_S_nspin=1.py': '_Sz.py'})
    #     elif inp.nspin == 3:
    #         templates_dict.update(
    #             {'bands_Sx_nspin=3.py': '_Sx.py',
    #             'bands_Sy_nspin=3.py': '_Sy.py',
    #             'bands_Sz_nspin=3.py': '_Sz.py'})
    #     for i, orb in enumerate(bands.orb_list):
    #         templates_dict.update({
    #             f'bands_Orb.py': '_O_{orb}.py' })

    dos.save_templates( templates_dict )
    
    # if bands.if_sphproj:
    #     for i, orb in enumerate(bands.orb_list):
    #         with open(f'_O_{orb}.py', 'r') as f:
    #             file = f.read()
    #         with open(f'_O_{orb}.py', 'w') as f:
    #             file.replace('orb_n = -1',f'orb_n = {i}')
    #             file.replace('orb = \'empty\'',f'orb = \'{orb}\'')
    #             f.write(file)

    # # delete massive variables
    # del(bands)

