#!/usr/bin/env python3
def read_inp():
    from read_func.inp_func import Inp
    # inp = Inp()
    try:
        inp = Inp()
    except:
        inp = None
        print('Can\' read INCAR and/or OUTCAR')
    return(inp)

def read_structure():
    from read_func.poscar_func import Poscar
    structure = Poscar()
    # try:
    #     structure = Poscar()
    # except:
    #     structure = None
    #     print('Can\' read POSCAR')
    return(structure)

def read_reciprocal():
    from read_func.kpoints_func import Kpoints
    reciprocal = Kpoints()
    # try:
    #     reciprocal = Kpoints()
    # except:
    #     reciprocal = None
    #     print('Can\' make rec lattice and/or read KPOINTS')
    return(reciprocal)
    # return(inp, structure, reciprocal)

inp = read_inp()
structure = read_structure()
reciprocal = read_reciprocal()
