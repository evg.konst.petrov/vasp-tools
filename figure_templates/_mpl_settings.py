#!/usr/bin/env python3
import matplotlib as mm
import matplotlib.pyplot as pp
import numpy as np
from _vasp_plot import *

fs = 7

# TeX settings
if False:
    mm.rcParams['text.usetex'] = True
    mm.rcParams['text.latex.preview'] = True
    mm.rcParams['font.family'] = 'cm'
    mm.rcParams['text.latex.preamble'] = r'''
    \usepackage[english]{babel}
    \usepackage[utf8x]{inputenx}
    \usepackage{amsmath}
    \usepackage{amsfonts}
    \usepackage{amssymb}
    \usepackage{ucs}
    \usepackage{cmbright}
    '''.split('\n')

# fontsizes, pads, etc.
pp.rc('font',   size=fs)
pp.rc('axes',   titlesize=fs, labelsize=fs, labelpad=1, titlepad=2)
pp.rc('xtick',  labelsize=fs - 1, direction='in')
pp.rc('ytick',  labelsize=fs - 1, direction='in')
pp.rc('legend', fontsize=fs - 3, markerscale=0.5 )
pp.rc('figure', titlesize=fs)

# colors
grid_color = 'grey'
spin_colors_up = ['red', 'gold', 'yellow']
spin_colors_dn = ['royalblue', 'green', 'cyan']
orb_colors = ['red', 'royalblue', 'gold', 'cyan',
              'green', 'magenta', 'lime', 'maroon',
              'chocolate', 'olive', 'navy', 'silver']