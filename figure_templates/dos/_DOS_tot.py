#!/usr/bin/env python3
import matplotlib as mm;  import matplotlib.pyplot as pp;  from matplotlib.gridspec import GridSpec
import numpy as np
from _vasp_plot import *
from _mpl_settings import *
from _info import *

# output name and format
output_name = name + '_DOS_tot'
output_format = '.pdf'

# fig size in cm
Sx = 4
Sy = 4.5

# figure margins (page size units): left, right, bottom, top
lm = 1.2/Sx
rm = 1 - 0.2/Sx
bm = 0.35/Sy
tm = 1 - 0.35/Sy

# axes limits
emin = emin - efermi
emax = emax - efermi

# yaxis dticks (spicing between the ticks)
dtick = (emax - emin)/4

# titles and note
title = name
ytitle = '$E - E_F$ (eV)'

# line widths: plot, axes and grid
lw = 0.2
lw_grid = lw*2

# line colors: plot, axes and grid
line_color = 'k'
ax_color = 'k'
grid_color = 'grey'

## ## ## ## HERE ALL MAGIC HAPPENS ## ## ## ##
### v v v v v v v v v v v v v v v v v v v v v
fig = pp.figure(figsize=(Sx/2.54, Sy/2.54))   # create figure and set its size
gs = GridSpec(1, 1, left=lm, bottom=bm, right=rm, top=tm)   # create plot grid
ax = pp.subplot(gs[0])   # create axes on the grid

egrid = np.load(egrid_file)['arr_0'] - efermi
DOS_tot = np.load(DOS_tot_file)['arr_0']
# plot total DOS
ax.fill_betweenx(egrid, DOS_tot, edgecolor='k', linewidth=0.2, facecolor=(0.5, 0.5, 0.5, 0.7))

# plot bands
ax.plot_zero(color=grid_color, lw=lw_grid, zorder=0.1)

# set title and labels
ax.set_title(title, loc='left') # set plot title
ax.set_ylabel(ytitle)

# set ticks and their labels
ax.yaxis.set_dtick(dtick)
ax.set_ticks_length(axis='x', length=0)
ax.yaxis.set_ticks_format('% 2.2F')

# set axes limits
ax.set_ylim(emin, emax)

# save
fig.savefig(output_name + output_format, dpi=400)
