#!/usr/bin/env python3
import matplotlib as mm;  import matplotlib.pyplot as pp;  from matplotlib.gridspec import GridSpec; import numpy as np
from _vasp_plot import *
from _mpl_settings import *
from _info import *

# output name and format
output_name = name + '_S<axis_name>'
output_format = '.pdf'

# fig size in cm
Sx = 8
Sy = 4.5

# axes limits
emin = -1
emax =  1
kmin =  0   # 0 = first, -1 = last
kmax = -1   # 0 = first, -1 = last

blocks = [range(N_at)]
block_names = ['']
axes = [[<axis_num>]]

# figure margins (page size units): left, right, bottom, top
lm = 1.0/Sx
rm = 1 - 0.2/Sx
bm = 0.35/Sy
tm = 1 - 0.35/Sy

# yaxis dticks (spacing between the ticks)
dtick = 0.5

# titles and note
title = name
ytitle = '$E - E_F$ (eV)'

# line widths: plot, axes and grid
lw = 0.2
lw_grid = lw*2
fat = 2

# line colors: plot, axes and grid
line_color = 'k'
ax_color = 'k'
grid_color = 'grey'

## ## ## ## HERE ALL MAGIC HAPPENS ## ## ## ##
fig = pp.figure(figsize=(Sx/2.54, Sy/2.54))   # create figure and set its size
gs = GridSpec(1, 1, left=lm, bottom=bm, right=rm, top=tm)   # create plot grid
ax = pp.subplot(gs[0])   # create axes on the grid

# load data
kpath = np.load(kpath_file)['arr_0']
E = np.load(E_file)['arr_0']
efermi = center_efermi(E, Ne, efermi)
E += -efermi
S =  np.load(S_file)['arr_0']



# plot bands
bands_to_draw = get_bands_to_draw(E, Nb, emin, emax)
# plot color lines (or circles)
S_sum = block_sum(S, blocks, axes)

for i_block in range(len(blocks)):
    # for i_axis in range(len(axes)):
    for b in bands_to_draw:
        s_up =   np.clip(S_sum[i_block, b], 0.0, np.max(S_sum[i_block, b]) )
        s_dn = - np.clip(S_sum[i_block, b], np.min(S_sum[i_block, b]), 0.0 )

        ax.plot_color_line(kpath, E[b], s_dn*fat, color=spin_colors_dn[i_block])
        ax.plot_color_line(kpath, E[b], s_up*fat, color=spin_colors_up[i_block])

ax.plot_bands(kpath, E, bands_to_draw, lw=lw, c=line_color)
ax.plot_zero(color=grid_color, lw=lw_grid, zorder=0.1)
ax.show_gap(E, Ne, color='lemonchiffon', zorder=0)


ax.add_legend_auto_spin(block_names, axes, spin_axes_list, spin_colors_up, spin_colors_dn)
leg = ax.legend(loc='upper left', frameon=True, fancybox=False, edgecolor='k')

# set title and labels
ax.set_title(title, loc='left') # set plot title
ax.set_ylabel(ytitle)

# set ticks and their labels
ax.set_xticks(kpath[kindex])
ax.xaxis.grid(True, lw=lw_grid) # add vertical grid
ax.yaxis.set_dtick(dtick)
ax.set_ticks_length(axis='x', length=0)
ax.set_xticklabels(knames) # set xaxis ticks labels
ax.yaxis.set_ticks_format('% 2.2F')

# set axes limits
ax.set_lim(kpath[kmin], kpath[kmax], emin, emax)

# save
fig.savefig(output_name + output_format, dpi=400)
