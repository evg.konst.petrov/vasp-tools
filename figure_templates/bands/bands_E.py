#!/usr/bin/env python3
import matplotlib as mm;  import matplotlib.pyplot as pp;  from matplotlib.gridspec import GridSpec
import numpy as np
from _vasp_plot import *
from _mpl_settings import *
from _info import *

# output name and format
output_name = name + '_E'
output_format = '.pdf'

# fig size in cm
Sx = 8
Sy = 4.5

# figure margins (page size units): left, right, bottom, top
lm = 1.0/Sx
rm = 1 - 0.2/Sx
bm = 0.35/Sy
tm = 1 - 0.35/Sy

# axes limits
kmin = 0                        # 0 = first, -1 = last
kmax = -1                       # 0 = first, -1 = last
emin = -2
emax = 2

# yaxis dticks (spicing between the ticks)
dtick = (emax - emin)/4

# titles and note
title = name
ytitle = '$E - E_F$ (eV)'

# line widths: plot, axes and grid
lw = 0.2
lw_grid = lw*2

# line colors: plot, axes and grid
line_color = 'k'
ax_color = 'k'
grid_color = 'grey'

## ## ## ## HERE ALL MAGIC HAPPENS ## ## ## ##
### v v v v v v v v v v v v v v v v v v v v v
fig = pp.figure(figsize=(Sx/2.54, Sy/2.54))   # create figure and set its size
gs = GridSpec(1, 1, left=lm, bottom=bm, right=rm, top=tm)   # create plot grid
ax = pp.subplot(gs[0])   # create axes on the grid

kpath = np.load(kpath_file)['arr_0']
E = np.load(E_file)['arr_0'] - efermi

# plot bands
bands_to_draw = get_bands_to_draw(E, Nb, emin, emax)
ax.plot_bands(kpath, E, bands_to_draw, lw=lw, c=line_color)
ax.plot_zero(color=grid_color, lw=lw_grid, zorder=0.1)

# set title and labels
ax.set_title(title, loc='left') # set plot title
ax.set_ylabel(ytitle)

# set ticks and their labels
ax.set_xticks(kpath[kindex])
ax.xaxis.grid(True, lw=lw_grid) # add vertical grid
ax.yaxis.set_dtick(dtick)
ax.set_ticks_length(axis='x', length=0)
ax.set_xticklabels(knames) # set xaxis ticks labels
ax.yaxis.set_ticks_format('% 2.2F')

# set axes limits
ax.set_lim(kpath[kmin], kpath[kmax], emin, emax)

# save
fig.savefig(output_name + output_format, dpi=400)
