#!/usr/bin/env python3
import matplotlib as mm;  import matplotlib.pyplot as pp;  from matplotlib.gridspec import GridSpec; import numpy as np
from _vasp_plot import *
from _mpl_settings import *
from _info import *

# output name and format
output_name = name + '_O_overall'
output_format = '.pdf'

kmin =  0   # 0 = first, -1 = last
kmax = -1   # 0 = first, -1 = last

orb_sets = [[0], [1], [2]]
if any(['f' in orb for orb in orb_list]):
    orb_sets.append([3])
blocks = blocks_by_type(at_list, at_types)
block_names = at_types

# fig size in cm
Sx = 8*len(orb_sets)
Sy = Sx*len(blocks)

# figure margins (page size units): left, right, bottom, top
lm = 1.0/Sx
rm = 1 - 0.2/Sx
bm = 0.35/Sy
tm = 1 - 0.35/Sy

# yaxis dticks (spacing between the ticks)
dtick = 0.5

# titles and note
title = name
ytitle = '$E - E_F$ (eV)'

# line widths: plot, axes and grid
lw = 0.1
lw_grid = lw*2
fat = 5

# line colors: plot, axes and grid
line_color = 'k'
ax_color = 'k'
grid_color = 'grey'

## ## ## ## HERE ALL MAGIC HAPPENS ## ## ## ##
fig = pp.figure(figsize=(Sx/2.54, Sy/2.54))   # create figure and set its size
gs = GridSpec(len(blocks), 3, left=lm, bottom=bm, right=rm, top=tm)   # create plot grid
# axex = [ pp.subplot(gs[i]) for i in range(len(blocks)) ]   # create axes on the grid

# load data
kpath = np.load(kpath_file)['arr_0']
E = np.load(E_file)['arr_0']
# efermi = center_efermi(E, Ne, efermi)
# E += -efermi
O =  np.load(O_file)['arr_0']

# axes limits
emin = E.min() - 0.5
emax = E.max() + 0.5

# plot bands
bands_to_draw = get_bands_to_draw(E, Nb, emin, emax)

# plot color lines (or circles)
for i_block in range(len(blocks)):
    
    for j, orb_set in enumerate( orb_sets ):
        orbs = [orb_set]*len(blocks)
        O_sum = block_sum(O, blocks, orbs)
        ax = pp.subplot(gs[i_block, j])
        for b in range(Nb):
            ax.plot_color_line(kpath, E[b], O_sum[i_block, b]*fat, orb_colors[i_block])


        ax.add_legend_auto_orb(block_names, orbs, orb_list, orb_colors)
        leg = ax.legend(loc='upper left', frameon=True, fancybox=False, edgecolor='k')

        # set title and labels
        ax.set_title(title, loc='left') # set plot title
        ax.set_ylabel(ytitle)

        # set ticks and their labels
        ax.set_xticks(kpath[kindex])
        ax.xaxis.grid(True, lw=lw_grid) # add vertical grid
        ax.yaxis.set_dtick(dtick)
        ax.set_ticks_length(axis='x', length=0)
        ax.set_xticklabels(knames) # set xaxis ticks labels
        ax.yaxis.set_ticks_format('% 2.2F')

        # set axes limits
        ax.set_xlim(kpath[kmin], kpath[kmax])
        ax.set_ylim(emin, emax)

# save
fig.savefig(output_name + output_format, dpi=600)
