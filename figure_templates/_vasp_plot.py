#!/usr/bin/env python3
import matplotlib as mm
import matplotlib.pyplot as pp
import numpy as np


def highlight_gap(self, E, Ne, **kwargs):
    gap_min = np.max(E[Ne-1])
    gap_max = np.min(E[Ne])
    if gap_min < gap_max:
        self.axhspan(gap_min, gap_max, **kwargs)
mm.axes.Axes.show_gap = highlight_gap


def center_efermi(E, Ne, efermi):
    gap_b = max(E[Ne-1])
    gap_t = min(E[Ne])
    gap = gap_t - gap_b
    if gap > 0.0:
        efermi += -(efermi - gap_b) + gap/2
    return(efermi)


def axes_color(self, color):
    for side in ['bottom', 'top', 'left', 'right']:
        self.spines[side].set_color(color)


mm.axes.Axes.axes_color = axes_color
mm.axes.Axes.axes_c = axes_color


def axes_linewidth(self, ax_lw):
    for side in ['bottom', 'top', 'left', 'right']:
        self.spines[side].set_linewidth(ax_lw)
    self.tick_params(axis='both', which='major', width=ax_lw)
    self.tick_params(axis='both', which='minor', width=ax_lw/2)


mm.axes.Axes.set_axes_linewidth = axes_linewidth
mm.axes.Axes.set_axes_lw = axes_linewidth


def ticks_direction(self, axis='both', direction='in'):
    self.tick_params(axis=axis, which='both', direction=direction)


mm.axes.Axes.set_ticks_direction = ticks_direction
mm.axes.Axes.set_ticks_dir = ticks_direction


def set_ticks_length(self, **kwargs):
    l = 4
    axis='both'
    for key, value in kwargs.items():
        if key == 'length':
            l = float(value)
        if key == 'axis':
            axis = value
    self.tick_params(axis=axis, which='major', length=l)
    self.tick_params(axis=axis, which='minor', length=l/2)
mm.axes.Axes.set_ticks_length = set_ticks_length
#mm.axes.Axes.set_ticks_l = set_ticks_length

def set_ticks_format(self, format='% 2.2F'):
    import matplotlib.ticker as ticker
    self.set_major_formatter(ticker.FormatStrFormatter(format))
mm.axis.XAxis.set_ticks_format = set_ticks_format
mm.axis.YAxis.set_ticks_format = set_ticks_format
#mm.axis.ZAxis.set_ticks_format = set_ticks_format

def set_dtick(self, dtick, dsubtick=-1):
    import matplotlib.ticker as ticker
    self.set_major_locator(ticker.MultipleLocator(base=dtick))
    if dsubtick == -1:
        self.set_minor_locator(ticker.MultipleLocator(base=dtick/2))
    elif dsubtick == 0:
        self.set_minor_locator(ticker.MultipleLocator(base=dtick))
    else:
        self.set_minor_locator(ticker.MultipleLocator(base=dsubtick))
mm.axis.XAxis.set_dtick = set_dtick
mm.axis.YAxis.set_dtick = set_dtick


def set_dsubtick(self, dtick):
    import matplotlib.ticker as ticker
    self.set_minor_locator(ticker.MultipleLocator(base=dsubtick))
mm.axis.XAxis.set_dsubtick = set_dsubtick
mm.axis.YAxis.set_dsubtick = set_dsubtick


# def xplaces(self, xticks, kpath, kpl):
#     if xticks == []:
#         tmp = range(len(kpath))
#         xticks = np.append(tmp[::kpl], tmp[-1])  # set xticks
#     self.set_major_locator(mm.ticker.FixedLocator(kpath[xticks]))


# mm.axis.XAxis.set_xticks = xplaces


def set_lim(self, xmin, xmax, ymin, ymax):
    self.set_xlim(xmin, xmax)
    self.set_ylim(ymin, ymax)
mm.axes.Axes.set_lim = set_lim


def add_legend_lines(self, labels, colors, lw=0.8):
    for i in range(len(labels)):
        self.plot([self.get_xlim()[0]-1], [self.get_ylim()[0]-1],
                  lw=lw, color=colors[i], label=labels[i])
mm.axes.Axes.add_legend_lines = add_legend_lines


def add_legend_markers(self, labels, colors, **kwargs):
    for i, label in enumerate(labels):
        self.scatter([self.get_xlim()[0]-1], [self.get_ylim()[0]-1],
                     color=colors[i], label=label, **kwargs)
mm.axes.Axes.add_legend_markers = add_legend_markers


def add_legend_auto_spin(self, block_names, axes, spin_axes_list, spin_colors_up, spin_colors_dn):
    labels = []
    colors = []
    for i, block_name in enumerate(block_names):
        axis_name = ''
        for j, axis in enumerate(axes[i]):
            axis_name += spin_axes_list[axis]
        labels.append(f'{block_name} $+s_' +'{' + axis_name + '}$')
        labels.append(f'{block_name} $-s_' +'{' + axis_name + '}$')
        colors += [spin_colors_up[i], spin_colors_dn[i]]
    self.add_legend_markers(labels, colors, marker='s')
mm.axes.Axes.add_legend_auto_spin = add_legend_auto_spin


def add_legend_auto_orb(self, block_names, orbs, orb_list, orb_colors):
    labels = []
    colors = []
    for i, block_name in enumerate(block_names):
        curr_orb_names = []
        for j, orb in enumerate(orbs[i]):
            curr_orb_names += [orb_list[orb]]
        orb_name = f"({'+'.join(curr_orb_names)})"

        colors += [ orb_colors[i] ]
        labels += [block_names[i] + ' ' + orb_name ]
    self.add_legend_markers(labels, colors, marker='s')
mm.axes.Axes.add_legend_auto_orb = add_legend_auto_orb


def get_bands_to_draw(E, Nb, emin=-2, emax=2):
    bands_to_draw = []
    for band in range(Nb):
        if np.max(E[band]) >= emin and np.min(E[band]) <= emax:
            bands_to_draw.append(band)
    return(bands_to_draw)


def plot_bands(self, kpath, E, bands_to_draw=None, **kwargs):
    if bands_to_draw == None:
        bands_to_draw = range(E.shape[0])
    for band in bands_to_draw:
        self.plot(kpath, E[band], **kwargs)


mm.axes.Axes.plot_bands = plot_bands


def plot_color_line(self, kpath, E_band, W_band, color):
    for i in np.arange(1, len(kpath)*2-1, 2):
        kpath = np.insert(kpath, i, np.average([kpath[i], kpath[i-1]]))
        E_band = np.insert(E_band, i, np.average([E_band[i], E_band[i-1]]))
        W_band = np.insert(W_band, i, np.average([W_band[i], W_band[i-1]]) )
    points = np.array([kpath, E_band]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    lc = mm.collections.LineCollection(
        segments, linewidths=W_band, color=color, zorder=0.1)
    lc.set_capstyle("round")
    self.add_collection(lc)


mm.axes.Axes.plot_color_line = plot_color_line


def plot_zero(self, e=0, **kwargs):
    self.plot(self.get_xlim(), [e, e], **kwargs)


mm.axes.Axes.plot_zero = plot_zero


def orb_sum_by_types(W, ion_nums):
    t = 0
    for typ in range(len(ion_nums)):
        W = np.sum(W[:, :, t:t+ion_nums[typ], :], axis=2)
        t += ion_nums[typ]
    return(W)


def blocks_by_type(at_list, at_types):
    blocks_by_type = []
    # ions_sorted_by_type.append(list(range(ion_nums[0])))
    for typ in at_types:
        blocks_by_type.append([])
        for i, atom in enumerate(at_list):
            if atom == typ:
                blocks_by_type[-1].append(i)
    return(blocks_by_type)


def block_sum(DATA, blocks, axes):
    # DATA is supposed to ahve shape: (N_at, N_orb, Nb, Nk)
    import sys
    if blocks == []:
        print('WARNING! blocks = [] detected')
        sys.exit()
    if str(type(blocks)) != "<class 'list'>":
        print(f"BLOCKS type is not list: {type(blocks)} ")
        sys.exit()
    if str(type(axes)) != "<class 'list'>" and str(type(axes)) == "<class 'int'>":
        axes = [[axes]]*len(blocks)
        print('AXES is integer, so it will be the same for each block.')
    
    DATA_sum = np.zeros((len(blocks), *DATA.shape[2:]))
    print(DATA.shape)
    for i, block, axis in zip(range(len(blocks)), blocks, axes):
        print(f'Summing data:\nblock = {block}\naxis = {axis}\n')
        DATA_sum[i] = np.sum( np.sum(DATA[block], axis=0)[axis], axis=0)
    return(DATA_sum)
        

def plot_proj(self, proj_master_dir, proj_list=[], **kwagrs):
    '''
    Fancy projection plotting function.

    Parameters:
    proj_master_dir (str): Path to dir containing all dirs with projections
    proj_list (list): List of floats (kz slices). Proj-dirs must have same names.
    
    '''

    # default values
    color = 'cadetblue'
    mode = 'fill'
    efermi_shift = 0.0
    dpi = 600
    redraw = False

    #TODO get kwargs

    print('Plotting projections...\n')
    if proj_list == []:
        proj_list_float = []
        import os
        for name in os.listdir(proj_master_dir):
            if not os.path.isdir(proj_master_dir + name):
                continue
            try:
                proj_list_float.append(name)
                proj_list.append(name)
            except:
                print(f'Directory name {name} cant be converted to float. Skipping.')
        ind_sort = np.argsort( np.array(proj_list_float) )
        proj_list_float = [proj_list_float[i] for i in ind_sort]
        proj_dir_list = [proj_master_dir + proj_list[i] for i in ind_sort]

    import imp
    p_info = imp.load_source('_info.py', proj_dir_list[0])
    E_p = np.ndarray((len(proj_dir_list), p_info.Nb, p_info.Nk))
    kpath_p = np.load(p_info.kpath_file)['arr_0']
    efermi_p = p_info.efermi
    for i, d in enumerate(proj_dir_list):
        p_info = imp.load_source('_info.py', d)
        E_p[i] = np.load(p_info.E_file)['arr_0']
    E_p -= ( efermi_p + efermi_shift)

    #TODO get bands to draw

    # create copy of the figure, on which ax exhists
    # pfig = 
    # pax = 

    # if redraw:
    #     for p in range(np.shape(Es)[0]-1): #projs[:-1]:
            
    #         # plot bands
    #         for band in bands_to_draw:
    #             ax.fill_between(kpath, Es[p, band], Es[p+1, band], color='cadetblue', zorder=0, lw=lw*3)
    #             ax.set_xlim(kpath[kmin], kpath[kmax])
    #             ax.set_ylim(emin, emax)
    #             #ax.plot(kpath, Es[p, band], '-', color='grey', lw=lw*2)
    #     ax.axis('off')
    #     extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    #     fig.savefig('proj.png', dpi=400, transparent=True, bbox_inches=extent, facecolor='w')
    #     ax.axis('on')
    # else:
    #     img = pp.imread('proj.png')
    #     ax.imshow(img, extent=[kpath[kmin], kpath[kmax], emin,emax], aspect="auto")

    if mode == 'fill':
        pass
    elif mode == 'lines':
        pass


    # pfig.clear()
    # pp.close(pfig)