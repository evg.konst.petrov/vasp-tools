#!/usr/bin/env python3
from read_func.func import *
import os
import numpy as np
from vasp_read import key_args, cwd
from read_jobs.read_input import inp, structure, reciprocal

class Bands():
    def __init__(self):
        self.if_sphproj = self.get_if_sphproj()
        self.if_procar_load = self.get_if_procar_load()
        if self.if_procar_load and self.if_sphproj:
            self.procar = self.get_procar()
        
        self.N_orb = 0
        self.orb_list = []
        self.spin_axes_list = self.get_spin_axes_list()
        self.efermi = self.get_bands_efermi()
        self.out_names = {
            'kpoints' : '_data_kpoints',
            'kpath' : '_data_kpath',
            'bands' : '_data_E',
            'orb' : '_data_O',
            'spin' : '_data_S'
            }
        self.templates_dir = 'figure_templates/bands'

    def get_if_sphproj(self):
        cond1 = os.path.exists('PROCAR')
        cond2 = '--noprocar' not in key_args \
                and '--no-sphproj' not in key_args
        return(cond1 and cond2)

    def get_if_procar_load(self):
            #return( '--load-full-procar' in key_args )
            return(False)

    def get_procar(self):
        if self.if_procar_load:
            print('Loading PROCAR...')
            procar = load('PROCAR')
            print('Done.\n')
        else:
            procar = None
        return(procar)

    def get_spin_axes_list(self):
        if inp.nspin == 3:
            spin_axes_list = ['x','y','z']
        elif inp.nspin == 1:
            spin_axes_list = ['z']
        else:
            print(f'inp.nspin = {inp.nspin}. Can\'t get spin_axes_list.')
            spin_axes_list = None
        return(spin_axes_list)

    def get_bands_efermi(self):
        is_efermi_arg_supplied = any( '-efermi' in arg for arg in key_args )
        if not is_efermi_arg_supplied:
            print('You have to specify E-fermi. Please type the path (relative or absolute) to directory with self-consistent OUTCAR or type E-fermi explicitly. Press Enter to use current directory.')
            sc_dir = input('Your choise: ')
        else:
            for arg in key_args:
                if '-efermi' in arg:
                    sc_dir = arg.split('=')[1]
        sc_dir = sc_dir.strip()
        if sc_dir in ['.','']:
            efermi = inp.get_efermi()
        else:
            try:
                efermi = float(sc_dir)
            except:
                from read_func.inp_func import Inp
                if os.path.isdir(sc_dir):
                    inp_sc = Inp(sc_dir)
                else:
                    filename = sc_dir.split('/')[-1]
                    sc_path_list = sc_dir.split('/')[:-1]
                    if len(sc_path_list) > 0:
                        sc_path = '/'.join( sc_path_list )
                    else:
                        sc_path = './'
                    inp_sc = Inp(sc_path, filename)
                efermi = inp_sc.get_efermi()
                del(inp_sc)

        print(f'E-fermi is set to {efermi} eV')
        return(efermi)

    @property
    def bands(self):
        print('Reading band structure...')
        if inp.ISPIN == 1:
            return(self.get_bands_ispin1())
        if inp.ISPIN == 2:
            return(self.get_bands_ispin2())

    def get_bands_ispin1(self):
        fermi_line = int(find_index_last('E-fermi', inp.file))
        kpoint_line = fermi_line + 3 + (inp.Nb + 2)*reciprocal.Nk_ignore
        lines_list = []
        for k in range(reciprocal.Nk):
            lines_list += list(range(kpoint_line + 2, kpoint_line + 2 + inp.Nb))
            kpoint_line += inp.Nb + 3
        E = np.loadtxt( [inp.file[i] for i in lines_list],
            usecols=1).reshape( 
            (inp.Nb, reciprocal.Nk), order='F' )
        return(E)
    
    def get_bands_ispin2(self):
        print('get_bands_ispin2 NOT YET IMPLEMENTED')
    
    @property
    def orb_and_spin(self):
        if self.if_procar_load:
            O = self.get_orb_defualt()
            S = self.get_spin_default()
        else:
            O, S = self.get_orb_and_spin_eco()
        return(O, S)

    def get_orb_defualt(self):
        print('Reading orbital composition...')
        self.orb_list = self.procar[7].split()[1:]
        self.N_orb = len(self.orb_list)
        if any('f-' in orb for orb in self.orb_list):
            if_f = 1
        else:
            if_f = 0
        O = np.ndarray((structure.N_at, self.N_orb, inp.Nb, reciprocal.Nk), dtype=np.float16)
        kpoint_line = 3
        lines_per_band = (structure.N_at + if_f + 1)*(1 + inp.nspin) + 4
        kpoint_line += (3 + lines_per_band)*reciprocal.Nk_ignore
        for k in range(reciprocal.Nk):
            print(f'Orbital composition: kpoint {k+1} / {reciprocal.Nk}', end='\r')
            band_line = kpoint_line + 2
            for b in range(inp.Nb):
                for at in range(structure.N_at):
                    # print(self.procar[band_line + 3 + at])
                    O[at, :, b, k] = self.procar[band_line + 3 + at].split()[1:]
                band_line += lines_per_band
            kpoint_line += lines_per_band*inp.Nb + 3
        print()
        return(O)

    def get_orb_and_spin_eco(self):
        print('Reading orbital composition and spin texture (eco mode)...')
        f = open(f'{cwd}/PROCAR', 'r')
        [f.readline() for i in range(7)]
        self.orb_list = f.readline().split()[1:]
        self.N_orb = len(self.orb_list)
        if_f = any('f-' in orb or 'fxyz' in orb for orb in self.orb_list)
        O = np.ndarray((structure.N_at, self.N_orb, inp.Nb, reciprocal.Nk))
        S = np.ndarray((structure.N_at, inp.nspin, inp.Nb, reciprocal.Nk))
        for k in range(reciprocal.Nk):
            print(f'Reading PROCAR: kpoint {k+1} / {reciprocal.Nk}', end='\r')
            for b in range(inp.Nb):
                for at in range(structure.N_at):
                    O[at, :, b, k] = f.readline().split()[1:]
                f.readline()
                if if_f:
                    f.readline()
                for ax in range(len(self.spin_axes_list)):
                    for at in range(structure.N_at):
                        S[at, ax, b, k] = f.readline().split()[-1]
                    f.readline()
                    if if_f:
                        f.readline()
                if inp.LORBIT == 12:
                    [ f.readline() for i in range(structure.N_at*2+1)  ]
                
                [f.readline() for i in range(4)]
            [f.readline() for i in range(3)]
        f.close()
        print()
        return(O, S)

    def get_spin_default(self):
        print('Reading spin texture...')
        if any('f-' in orb for orb in self.orb_list):
            if_f = 1
        else:
            if_f = 0

        if inp.nspin == 3:
            self.spin_axes_list = ['x', 'y', 'z']
            S = np.ndarray((structure.N_at, inp.nspin, inp.Nb, reciprocal.Nk))
            kpoint_line = 3
            lines_per_band = (structure.N_at + if_f + 1)*(1+inp.nspin) + 4
            kpoint_line += (3 + lines_per_band)*reciprocal.Nk_ignore
            for k in range(reciprocal.Nk):
                print(f'Spin texture: kpoint {k+1} / {reciprocal.Nk}', end='\r')
                band_line = kpoint_line + 2
                for b in range(inp.Nb):
                    for ax in range(inp.nspin):
                        for at in range(structure.N_at):
                            # print(self.procar[band_line + 3 + at + (structure.N_at + 1 + if_f)*(ax+1) ])
                            S[at, ax, b, k] = self.procar[band_line + 3 + at + (structure.N_at + 1 + if_f)*(ax+1) ].split()[-1]
                    band_line += lines_per_band
                kpoint_line += lines_per_band*inp.Nb + 3
            print()
            return(S)

        elif inp.nspin == 1:
            print('spin texture for inp.nspin=1 is not implemented yet')
            return(None)

        elif inp.nspin == 0:
            print('It looks like there is no spin texture (inp.nspin = 0)')
            return(None)

        print()

    def save_templates(self, templates_list):
        from __main__ import kit_dir
        for template, file in templates_list:
            os.system(f'cp {kit_dir}/{self.templates_dir}/{template} ./{file} ')
        os.system(f'cp {kit_dir}/figure_templates/_vasp_plot.py ./_vasp_plot.py ')
        os.system(f'cp {kit_dir}/figure_templates/_mpl_settings.py ./_mpl_settings.py ')