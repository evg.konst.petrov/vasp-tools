#!/usr/bin/env python3
from read_func.func import *
import numpy as np
from read_jobs.read_input import inp, structure
# from vasp_read import inp, structure

class Kpoints:
    def __init__(self):
        
        self.file = load('KPOINTS')
        self.mode = self.get_mode()
        self.rec_basis = np.linalg.inv(structure.basis.T)
        self.rec_basis_a = np.linalg.inv(structure.basis_a.T)
        self.Nk_ignore = self.get_Nk_ignore()
        self.Nk = self.get_Nk()
        self.kpoints = self.get_kpoints()
        self.knames = self.get_kpoints_names()
        self.kindex = self.get_spec_k()


    def get_mode(self):
        if 'L' in self.file[2]:
            mode = 'line'
        elif any(s in self.file[2] for s in ['rec','Rec']):
            mode = 'explicit'
        elif '0' in self.file[1]:
            mode = 'bz'
        return(mode)

    def get_Nk_ignore(self):
        Nk_ignore = 0
        from __main__ import kit_mode
        if kit_mode != 'read':
            return(Nk_ignore)
        
        from vasp_read import job
        if job == 'bands':
            if inp.METAGGA == '.T.' or inp.LHFCALC == '.T.':
                print('WARNING!')
                print('It seems like you enabled MetaGGA or exact exhange contibution for XC functional. \nNow you have to set the number of k-points, which will be excluded from band structure.')
                # Check these k-points in KPOINTS
                # Find line, where actual band k-kpoints start
                for i, line in enumerate(self.file):
                    if float(line.split()[3]) == 0.0:
                        Nk_ignore = i-3
                        break
                # Promt input if failed
                if Nk_ignore != 0:
                    print(f'Nk_ignore is set to {Nk_ignore}.')
            else:
                Nk_ignore = 0
        return(Nk_ignore)

    def get_Nk(self):
        try:
            return( inp.Nk - self.Nk_ignore )
        except:
            pass

    def get_kpoints(self):
        OUTCAR = inp.file
        index = find_index_1(
            'k-points in reciprocal lattice and weights:', OUTCAR) + 1
        kpoints = np.loadtxt(OUTCAR[index: index + self.Nk + self.Nk_ignore], usecols=(0,1,2), dtype=np.float16)
        print(f'Found {self.Nk} kpoints')
        return(kpoints)

    def get_kpoints_names(self):
        names = []
        # Check whether the line mode used.
        if self.mode == 'line':
            if len(self.file[-1].split()) >= 4:
                names = np.loadtxt(self.file[4:], usecols=(3), dtype=str)
                # remove duplicating names
                del_list = []
                for i in range(len(names)-1):
                    if names[i] == names[i+1]:
                        del_list.append(i)
                    else:
                        pass
                names = np.delete(names, del_list)
                print('Following named k-points detected:',
                        ' '.join(map(str, names)))
            else:
                print(
                    'Number of columns if KPOINTS file is inconsistent. Can\'t get k-points names.')
        # Check whether all k-points specified explicilty, like for HSE or MetaGGA
        if self.mode == 'explicit':
            if len(self.file[-1].split()) >= 5:
                # Find line, where actual band k-kpoints start
                tmp = []
                for i in range(3, len(self.file)):
                    if int(float(self.file[i].split()[3])) == 0:
                        break
                for j in range(i, len(self.file)):
                    if len(self.file[j].split()) == 5:
                        tmp.append(self.file[j])
                names = np.loadtxt(tmp, usecols=(4), dtype=str)

                # remove duplicating names
                del_list = []
                for i in range(len(names)-1):
                    if names[i] == names[i+1]:
                        del_list.append(i)
                    else:
                        pass
                names = np.delete(names, del_list)

                print('Following named k-points detected:' + \
                            ' '.join(map(str, names)))
            else:
                print('Number of columns if KPOINTS file is inconsistent. Can\'t get k-points names.')
        return( list(names) )

    def get_spec_k(self):
        kindex = []
        if self.mode == 'line':
            kpl = int(self.file[1])
            kindex = list(range(0, self.Nk, kpl))
        elif self.mode == 'explicit':
            kindex.append(0)
            kindex.append(self.Nk-1)
            for i in range(1, self.Nk-1):
                if all(self.kpoints[i, j] == self.kpoints[i+1, j] for j in range(3)):
                    kindex.append(i+1)
                else:
                    dk1 = self.kpoints[i+1] - self.kpoints[i]
                    dk2 = self.kpoints[i] - self.kpoints[i-1]
                    if all(np.cross(dk1, dk2)[j] != 0.0 for j in range(3) ):
                        print(f'k-line break detected at point # {i}')
                        kindex.append(i)
        kindex.append(self.Nk -1)
        return(kindex)

    @property
    def kpath(self):
        kpath = []
        kpath.append(0.0)
        for k in range(1, self.Nk):
            tmp = np.linalg.norm(((self.kpoints[k] - self.kpoints[k-1]) @ self.rec_basis_a)) + kpath[-1]
            kpath.append( tmp )
        kpath = np.array(kpath, dtype=np.float32) *np.pi*2
        return(kpath)



