#!/usr/bin/env python3
from read_func.func import *
import os
import numpy as np
from vasp_read import key_args
from read_jobs.read_input import inp, structure

class Dos():
    def __init__(self, filename='DOSCAR'):
        self.if_sphproj = self.get_if_sphproj()
        self.file = self.get_file(filename)
        self.N_orb = 0
        self.orb_list = self.get_orb_list()
        # self.nep = self.get_nep()
        self.egrid = self.get_egrid()
        self.dos = self.get_dos()
        self.dos_orb = self.get_dos_orb()
        self.dos_slms = self.get_dos_slms()
        # self.dos_spin = self.get_dos_spin()
        self.out_names = {
            'dos' : '_data_DOS',
            'dos_orb' : '_data_DOS_orb',
            'dos_spin' : '_data_DOS_spin',
            'dos_slms' : '_data_DOS_slms',
            'egrid' : '_data_egrid'
            }
        self.templates_dir = f'figure_templates/dos'
    
    def get_if_sphproj(self):
        cond1 = inp.LORBIT != 0
        cond2 = '--no-sphproj' not in key_args
        return(cond1 and cond2)

    def get_file(self, filename):
        file = load(filename)
        self.nep = self.get_nep()
        N_at = structure.N_at

        if inp.LMAXMIX == 6 and self.if_sphproj:
            print('LMAXMIX = 6 detected! In this case VASP priduces quite shitty DOSCAR format. Trying to fix it...')
            file = ''.join(file).split('\n')
            print(f'Old length: {len(file)}')
            start_line = 7 + self.nep
            file_new = []
            lines_per_atom = self.nep*2 + 1
            for a in range(N_at):
                for n in range(lines_per_atom):
                    file_new.append(' '.join( file[ start_line + n : start_line + n + 2] ))
                    # file[ start_line + n : start_line + n + 2] = \
                        # [ ' '.join( file[ start_line + n : start_line + n + 2] ) ]
                    # print(file_new[-1])
                start_line += lines_per_atom
            print(f'New length: {len(file_new)}')
            # print('\n')
        return(file)

    def get_orb_list(self):
        if inp.LORBIT == 11 and inp.LMAXMIX == 6:
            orb_list = ['s', 'py', 'pz', 'px', 'dxy', 'dyz', 'dz2', 'dxz', 'dx2', 'f-3', 'f-2', 'f-1', 'f0', 'f1', 'f2', 'f3']
        elif inp.LORBIT == 10 and inp.LMAXMIX == 6:
            orb_list = ['s', 'p', 'd', 'f']
        elif inp.LORBIT == 11 and inp.LMAXMIX == 4:
            orb_list = ['s', 'py', 'pz', 'px', 'dxy', 'dyz', 'dz2', 'dxz', 'dx2']
        elif inp.LORBIT == 10 and inp.LMAXMIX == 4:
            orb_list = ['s', 'p', 'd']
        else:
            print(f'Can\' get orbitals list for DOS!\n LMAXMIX = {inp.LMAXMIX}, LORBIT = {inp.LORBIT}' )
            orb_list = None
        return(orb_list)

    def get_nep(self):
        nep = int(find_line_1('NEDOS', inp.file_head).split()[5])
        return(nep)

    def get_egrid(self):
        start_line = 6
        egrid = np.loadtxt(self.file[start_line : start_line + self.nep], usecols=(0))
        return(egrid)
    
    def get_dos(self):
        start_line = 6
        dos = np.loadtxt(self.file[start_line : start_line + self.nep], usecols=(1))
        return(dos)

    def get_dos_orb(self):
        N_at = structure.N_at
        N_orb = len(self.orb_list)
        nep = self.nep
        dos_orb = np.ndarray((N_at, N_orb, nep))
        print()
        start_line = 7 + nep
        if inp.LMAXMIX == 4:
            dlmaxmix = -1
        else:
            dlmaxmix = 0

        for a in range(N_at):
            print(f'Reading orb DOS on atom {a+1}/{N_at}...', end='\r')

            if inp.LMAXMIX == 4:
                cols = tuple(range(1, 37, 4))
                end_line = start_line + nep 
                dos_orb[a, 0:9] = np.loadtxt(
                    self.file[start_line : end_line],
                    usecols=cols).T
                start_line = end_line + 1

            if inp.LMAXMIX == 6:
                # print(self.file[start_line].split())
                cols = tuple(range(1, 37, 4))
                # print(cols)
                end_line = start_line + nep*2 - 1
                dos_orb[a, 0:9] = np.loadtxt(
                    self.file[start_line : end_line : 2],
                    usecols=cols).T

                cols = tuple(range(0, 28, 4))
                end_line = start_line + nep*2 - 1
                # print(end_line)
                dos_orb[a, 9:17] = np.loadtxt(
                    self.file[start_line+1 : end_line +1: 2],
                    usecols=cols).T
                start_line = end_line + 2
        print()
        return(dos_orb)

    def get_dos_slms(self):
        N_at = structure.N_at
        N_orb = len(self.orb_list)
        nep = self.nep
        dos_slms = np.ndarray((N_at, N_orb, inp.nspin, nep))
        print()
        start_line = 7 + nep
        if inp.LMAXMIX == 4:
            dlmaxmix = -1
        else:
            dlmaxmix = 0

        for a in range(N_at):
            print(f'Reading slms DOS on atom {a+1}/{N_at}...', end='\r')
            if inp.LMAXMIX == 4:
                for s in range(inp.nspin):
                    cols = tuple(range(2+s, 37, 4))
                    end_line = start_line + nep 
                    dos_slms[a, 0:9, s] = np.loadtxt(
                        self.file[start_line : end_line],
                        usecols=cols).T
                start_line = end_line + 1

                if inp.LMAXMIX == 6:
                    # print(self.file[start_line].split())
                    cols = tuple(range(2+s, 37, 4))
                    # print(cols)
                    end_line = start_line + nep*2 - 1
                    dos_slms[a, 0:9, s] = np.loadtxt(
                        self.file[start_line : end_line : 2],
                        usecols=cols).T

                    cols = tuple(range(0, 28, 4))
                    end_line = start_line + nep*2 - 1
                    # print(end_line)
                    dos_slms[a, 9:17, s] = np.loadtxt(
                        self.file[start_line+1 : end_line +1: 2],
                        usecols=cols).T
                    start_line = end_line + 2
        print()
        return(dos_slms)

    def get_dos_spin(self):
        N_at = structure.N_at
        nep = self.nep
        dos_spin = np.ndarray((N_at, inp.nspin, nep))
        print()
        start_line = 7 + nep
        if inp.LMAXMIX == 4:
            dlmaxmix = -1
        else:
            dlmaxmix = 0

        for a in range(N_at):
            print(f'Reading spin DOS on atom {a+1}...', end='\n')

            if inp.LMAXMIX == 4:
                cols = tuple(range(1, 37, 4))
                end_line = start_line + nep 
                dos_orb[a, 0:9] = np.loadtxt(
                    self.file[start_line : end_line],
                    usecols=cols).T
                start_line = end_line + 1

            if inp.LMAXMIX == 6:
                print(self.file[start_line].split())
                cols = tuple(range(1, 37, 4))
                print(cols)
                end_line = start_line + nep*2 - 1
                dos_orb[a, 0:9] = np.loadtxt(
                    self.file[start_line : end_line : 2],
                    usecols=cols).T

                cols = tuple(range(0, 28, 4))
                end_line = start_line + nep*2 - 1
                print(end_line)
                dos_orb[a, 9:17] = np.loadtxt(
                    self.file[start_line+1 : end_line +1: 2],
                    usecols=cols).T
                start_line = end_line + 2
        print()
        return(dos_spin)

    def save_templates(self, templates_dict):
        from __main__ import kit_dir
        for template in templates_dict:
            os.system(f'cp {kit_dir}/{self.templates_dir}/{template} ./{templates_dict[template]} ')
        os.system(f'cp {kit_dir}/figure_templates/_vasp_plot.py ./_vasp_plot.py ')
        os.system(f'cp {kit_dir}/figure_templates/_mpl_settings.py ./_mpl_settings.py ')


