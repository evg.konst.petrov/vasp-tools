#!/usr/bin/env python3
from read_func.func import *
import numpy as np

class Poscar():
    def __init__(self, poscar_filename='POSCAR'):
        self.file = self.get_poscar(poscar_filename)
        self.name = self.file[0].strip()
        self.latt_a = float(self.file[1])
        self.basis = np.loadtxt(self.file[2:5])
        self.basis_a = self.basis*self.latt_a
        self.cell_volume = np.linalg.det(self.basis_a)
        self.at_types = self.file[5].split()
        self.at_numbers = list(np.array(self.file[6].split(), dtype=np.int))
        self.N_at = np.sum(self.at_numbers)
        self.at_pos_dir, self.at_pos_cart = self.get_pos()
        self.at_list = self.get_at_list()

    def get_poscar(self, file):
        with open(file, 'r') as f:
            poscar = f.readlines()
        if any(s in poscar[7] for s in ['Cart', 'cart', 'Dir', 'dir']):
            poscar.insert(7, ' ')
        return(poscar)

    def get_at_list(self):
        at_list = []
        for number, typ in zip(self.at_numbers, self.at_types):
            at_list += [typ]*number
        return(at_list)

    def get_pos(self):
        start = 7
        for i, line in enumerate(self.file[start:]):
            if 'Direct' in line or 'direct' in line:
                cart = False
                break
            elif 'Cart' in line or 'cart' in line:
                cart = True
                break
        start = start + i + 1
        at_pos_dir = np.loadtxt(self.file[start:start+np.sum(self.at_numbers)], usecols=(0,1,2))
        if not cart:
            at_pos_cart = np.dot(at_pos_dir, self.basis_a)
        else:
            pass

        return(at_pos_dir, at_pos_cart)
    
    def get_trans_vectors(self):
        trans_vectors = []
        for z in [0,-1,1]:
            for y in [0,-1,1]:
                for x in [0,-1,1]:
                    vec = [x,y,z]
                    if [x,y,z] not in [trans_vectors]:# and [x,y,z] != [0,0,0]:
                        trans_vectors.append([x,y,z])
        # trans_vectors.insert(0, [0,0,0])
        trans_vectors = np.array(trans_vectors, dtype=np.float)
        return(trans_vectors)

    def get_pos_trans(self, set_atoms_pos = True):
        pos_dir_trans_cell = []
        for vec in self.get_trans_vectors():
            for i, atom in enumerate(self.atoms):
                pos_dir_trans = atom.pos_dir + vec
                pos_dir_trans_cell.append(pos_dir_trans)
                
                if set_atoms_pos:
                    atom.pos_dir_trans.append(pos_dir_trans)
                    atom.pos_cart_trans.append(pos_dir_trans @ self.basis_a)
        return(pos_dir_trans_cell, pos_dir_trans_cell @ self.basis_a)

    def get_coo_spheres(self):
        '''Get first 4 coordination spheres radii'''
        tol_digits = 5
        num_spheres = 4
        from scipy.spatial import cKDTree
        tree = cKDTree(self.at_pos_cart_trans)
        neis_dist, neis_ind = tree.query(self.at_pos_cart, k=100,
                                         distance_upper_bound=self.latt_a*2)
        
        neis_dist_unique = []
        neis_ind_unique = []
        print('Coordination spheres radii are calculated:')
        print(f'Atom    Coo radii')
        for i, atom in enumerate(self.atoms):
            neis_dist_unique.append([])
            neis_ind_unique.append([])
            for j in range(len(neis_dist[i])):
                r = np.round( neis_dist[i][j], tol_digits )
                if r not in neis_dist_unique[i]:
                    neis_dist_unique[i].append(r)
                    neis_ind_unique[i].append(neis_ind[i][j])
            atom.coo_radii = neis_dist_unique[i][1:num_spheres+1]
            atom.coo_atoms_ind_trans = neis_ind_unique[i][1:num_spheres+1]
            atom.coo_atoms_ind = np.array( list(range(self.N_at))*len(self.get_trans_vectors()) )[atom.coo_atoms_ind_trans]
            print(f'{atom.name:<3}     {np.round(atom.coo_radii, 6)}')

    # def get_rmt(self, atoms, like_kkr=False):
    #     '''Get non-overlapping (touching) MT radii
    #     regardless of atoms sort or whatever'''
    #     from scipy.spatial import cKDTree
    #     tree = cKDTree(self.at_pos_cart_trans)
    #     neis_dist, neis_ind = tree.query(self.at_pos_cart, k=[2],
    #                                      distance_upper_bound=5)
    #     neis_dist = neis_dist.flatten()
    #     neis_ind = neis_ind.flatten()
    #     neis_r_sorted = np.argsort(neis_dist)

    #     for i in neis_r_sorted:
    #         atom = self.atoms[i]
    #         dist = neis_dist[i]
    #         ind = neis_ind[i]
    #         equiv_nei_index = self.equivalents_to[ind]
    #         r_of_eqiv = self.atoms[equiv_nei_index].rmt
    #         if r_of_eqiv == 0.0:
    #             atom.rmt = dist/2
    #         else:
    #             if like_kkr:
    #                 # this is how it is in KKR
    #                 atom.rmt = dist/2
    #             else:
    #                 atom.rmt = dist - r_of_eqiv

    # def get_voronoi(self, set_vor_vol = True):
    #     from scipy.spatial import Voronoi
    #     vor = Voronoi(self.at_pos_cart_trans)
    #     print('\nVoronoi polyhdra were constructed.')
    #     return(vor)

    # def get_rws_voronoi(self, set_rws = True):
    #     from scipy.spatial import ConvexHull
    #     vor = self.voronoi
    #     vor_vertices = vor.vertices
    #     vor_regions = vor.regions
    #     vor_indicies = vor.point_region[:self.N_at]
    #     rws_vor_radii = []
    #     print(f'\nWS radii are obtained for each atominc position via Voronoi volumes.\n')
    #     print(f'Atom    Vor_V (A^3)    Vor_R (A)    Vor_R (au)')
    #     for i, atom in enumerate(self.atoms):
    #         poly = ConvexHull( vor_vertices[vor_regions[ vor.point_region[i] ]] )
    #         atom.vor_vol = poly.volume
            
    #         vor_r = (atom.vor_vol/4*3/np.pi)**(1/3)
    #         rws_vor_radii.append(vor_r)
    #         atom.rws_vor = vor_r
    #         print(f'{atom.name:<3}     {round(atom.vor_vol, 6):<9}      {round(vor_r, 6):<8}     {round(vor_r/0.5291772083, 6):<8}')
    #     print('')

    # def get_rmt_voronoi(self):
    #     from scipy.spatial import ConvexHull
    #     vor = self.voronoi
    #     vor_vertices = vor.vertices
    #     vor_regions = vor.regions
    #     vor_indicies = vor.point_region[:self.N_at]
        
    #     print(f'\nMT radii are obtained for each atominc position via Voronoi polyhedra.\n')
    #     print(f'Atom    Vor_rmt (A)    Vor_rmt (au)')
    #     for i, atom in enumerate(self.atoms):
    #         poly = ConvexHull( vor_vertices[vor_regions[ vor_indicies[i] ]] )
    #         eqs = poly.equations
            
    #         radii = np.array( np.abs(
    #             [ ( np.sum(eq[:3]*atom.pos_cart) + eq[3] ) / \
    #                 np.linalg.norm(eq[:3]) for eq in eqs ] ) )
    #         rmt_vor = np.min(radii)
    #         atom.rmt_vor = rmt_vor
    #         print(f'{atom.name:<3}      {round(rmt_vor, 6):<8}     {round(rmt_vor/0.5291772083, 6):<8}')

    # def get_rmt_touching(self, verb=True, set_radii=True):
    #     import time
    #     rmt_radii = [atom.rmt_vor for atom in self.atoms]
    #     tol = 1e-6
    #     rmt_radii_rescaled = np.array(rmt_radii)

    #     cell_overlaps = self.get_overlaps(rmt_radii, True, 3)
    #     max_overlaps = np.max(cell_overlaps, axis=1)
    #     tot_overlap = np.min(max_overlaps)
    #     while tot_overlap < tol*(-1):
    #         print('Tot overlap: ', tot_overlap)
    #         atoms_to_rescale = []
    #         dr = []
    #         for i, atom in enumerate(self.atoms):
    #             if np.abs(max_overlaps[i]) > tol:
    #                 biggest_nei = np.argmax(cell_overlaps[i])
    #                 biggest_nei_index = atom.coo_atoms_ind[biggest_nei]
    #                 coo_radius = atom.coo_radii[biggest_nei]
    #                 dr.append(coo_radius - rmt_radii_rescaled[i] - rmt_radii_rescaled[biggest_nei_index])
    #             else:
    #                 dr.append(0)
    #         rmt_radii_rescaled += np.array(dr)/2 - tol/4

    #         cell_overlaps = self.get_overlaps(rmt_radii_rescaled, False, 3)
    #         max_overlaps = np.max(cell_overlaps, axis=1)
    #         tot_overlap = np.min(max_overlaps)
    #     cell_overlaps = self.get_overlaps(rmt_radii_rescaled, verb, 3)
    #     if set_radii:
    #         for i, atom in enumerate(self.atoms):
    #             atom.rmt = rmt_radii_rescaled[i]
    #     return(rmt_radii_rescaled)

    # def get_rws_from_rmt(self, rmt_radii=[], verb=True, set_radii=True):
    #     if rmt_radii == []:
    #         rmt_radii = np.array([atom.rmt_vor for atom in self.atoms])
    #     Vc = self.cell_volume
    #     rws_radii = np.array(rmt_radii)
    #     Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
    #     dV = Vrws/Vc
    #     while np.abs(dV - 1) >= 1e-15 :
    #         dV = Vrws/Vc
    #         rws_radii /= dV**0.333
    #         Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
    #     if set_radii:
    #         if verb:
    #             print('WS radii were obtained from MT radii (just scaled)')
    #             print(f'Cell volume: {Vc}, Sum of spheres volumes: {Vrws}')
    #             print(f'Atom      rws (A)      rws (au)')
    #         for i, atom in enumerate(self.atoms):
    #             atom.rws = rws_radii[i]
    #             if verb:
    #                 print(f'{atom.name:<3}      {round(rws_radii[i], 6):<8}     {round(rws_radii[i]/0.5291772083, 6):<8}')
    #     return(rws_radii)

    # def get_rws_remaining(self, rws_radii=[], verb=True, set_radii=True):
    #     if rws_radii == []:
    #         print('No radii set, so ALL WS-radii will be calculated.')
    #         return(self.get_rws_voronoi())
    #     elif len(rws_radii) != self.N_at:
    #         dn = self.N_at - len(rws_radii)
    #         print(f'Last {dn} radii were not set, so they are assumed to be 0.')
    #         while len(rws_radii) != self.N_at:
    #             rws_radii.append(0.0)

    #     else:
    #         at_ind = []
    #         for i, atom in enumerate(self.atoms):
    #             if rws_radii[i] <= 0.0:
    #                 at_ind.append(i)
    #                 rws_radii[i] = atom.rmt_vor
    #         Vc = self.cell_volume
    #         rws_radii = np.array(rws_radii)
    #         Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
    #         dV = Vrws/Vc
    #         print(f'Following atoms radii will be calculated: {at_ind}. \nOthers will be kept fixed.')
    #         while np.abs(dV - 1) >= 1e-15 :
    #             dV = Vrws/Vc
    #             # print(rws_radii[at_ind])
    #             rws_radii[at_ind] /= dV**0.333
    #             Vrws = np.sum( [4*np.pi/3*r**3 for r in rws_radii] )
    #         if set_radii:
    #             if verb:
    #                 print('Remaining WS radii were calculated (just scaled)')
    #                 print(f'Cell volume: {Vc}, Sum of spheres volumes: {Vrws}')
    #                 print(f'Atom      rws (A)      rws (au)')
    #             for i, atom in enumerate(self.atoms):
    #                 atom.rws = rws_radii[i]
    #                 if verb:
    #                     print(f'{atom.name:<3}      {round(rws_radii[i], 6):<8}     {round(rws_radii[i]/0.5291772083, 6):<8}')
    #         return(rws_radii)