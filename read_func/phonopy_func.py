#!/usr/bin/env python3
from read_func.func import *
import numpy as np
import yaml
# from vasp_read import inp, structure, reciprocal

class Phonopy_bands():
    def __init__(self, filename):
        self.yamldata = self.get_yaml(filename)
        self.Nq = int( self.yamldata['nqpoint'] )
        self.rec_basis = np.array(self.yamldata['reciprocal_lattice'])
        self.N_at = self.yamldata['natom']
        self.out_names = {
            'qpoints' : '_data_qpoints',
            'qpath' : '_data_qpath',
            'bands' : '_data_E'
            }
        self.templates_dir = f'figure_templates/phonon_bands'

    def get_yaml(self, filename):
        with open(filename, 'r') as f:
            data = yaml.load(f)
        return(data)

    def get_qpoints(self):
        qpoints = np.ndarray((self.Nq, 3))
        tmp = self.yamldata['phonon']
        for i in range(self.Nq):
            qpoints[i] = tmp[i]['q-position']
        return(qpoints)

    def get_spec_q(self):
        spec_q_index = [0]
        spec_q_index += list(np.cumsum( self.yamldata['segment_nqpoint'] ))
        spec_q_index[-1] += -1
        return(spec_q_index)

    def get_qpath(self):
        qpoints = self.get_qpoints()
        rec_basis = self.rec_basis
        dqpath = np.sum( ((qpoints[1:] - qpoints[:-1]) @ rec_basis)**2, axis=1)**0.5
        qpath = np.cumsum( np.insert(dqpath, 0, 0) )
        print(qpath)
        return(qpath)

    def get_ph_bands(self):
        freqs = np.ndarray((self.N_at*3, self.Nq))
        tmp = self.yamldata['phonon']
        for i in range(self.Nq):
            for j, band in enumerate( tmp[i]['band'] ):
                freqs[j, i] = band['frequency']
        return(freqs)

    # def get_phbands_proj(self):
    #     ph_bands_proj = np.ndarray((self.Nq, structure.N_at*3, 3))
    #     tmp = self.yamldata['phonon']
    #     for i in range(self.Nq):
    #         for j, band in enumerate( tmp[i]['band'] ):
    #             for k in range(3):
    #                 ph_bands_proj[i, j, k] = 
            
    def save_templates(self, templates_list):
        from __main__ import kit_dir
        for template, file in templates_list:
            os.system(f'cp {kit_dir}/{self.templates_dir}/{template} ./{file} ')
        os.system(f'cp {kit_dir}/figure_templates/_vasp_plot.py ./_vasp_plot.py ')
        os.system(f'cp {kit_dir}/figure_templates/_mpl_settings.py ./_mpl_settings.py ')
