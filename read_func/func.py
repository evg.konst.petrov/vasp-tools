#!/usr/bin/env python3
import os

def load(filename):
    with open(filename, 'r') as f:
        file = f.readlines()
        return(file)

def find_line_1(string, file):
    for line in file:
        if string in line:
            return(line)
            break

def find_lines(string, file):
    lines = []
    for line in file:
        if string in line:
            lines.append(line)
    return(lines)

def get_val(param, file):
    '''
    function to get certain parameter value 
    from list (file).
    returns string!
    '''
    lines = find_lines(param, file)
    if len(lines) > 1:      
        # if we have several matches, 
        #some paramter may have close name (like etype and qetype)
        for line in lines:
            param_s = ' ' + param
            if param in lines[0]:
                val = lines[0].split('=')[1].split()[0]
                if val[-1] == ';':
                    val = val[:-1]
                return(val)
                break
    elif len(lines) == 1:
        if param in lines[0]:
            val = lines[0].split('=')[1].split()[0]
            if val[-1] == ';':
                val = val[:-1]
            return(val)
    elif len(lines) == 0:
        print(f'Parameter {param} is not in file.')
        return('')
    
def get_all_vals(param, file):
    '''
    function to get certain parameter value 
    from list (file).
    returns string!
    '''
    vals = []
    lines = find_lines(param, file)
    if len(lines) >= 1:
        for line in lines:
            vals.append(str2val(param, line))
        return(vals)
    elif len(lines) == 0:
        print(f'Parameter {param} is not in file.')
        return('')

def find_index_1(what, file):
    for i in range(len(file)):
        if what in file[i]:
            tmp = i
            break
        else:
            tmp = None
    return(tmp)

def find_index_last(what, file):
    for i in range(len(file))[::-1]:
        if what in file[i]:
            tmp = i
            break
        else:
            tmp = None
    return(tmp)

def save_templates(templates_dict):
    from vasp_read import kit_dir
    for template in templates_dict:
        os.system(f'cp {kit_dir}/fig_templates/{template} ./{templates_dict[template]} ')

def write_info(info_dict):
    with open('_info.py', 'w') as w:
        w.write('#!/usr/bin/env python3\n')
        w.write('import numpy as np\n')
        for info in info_dict:
            tmp = info_dict[info]
            if type(tmp) == str and 'np.' not in tmp:
                tmp = f"\'{tmp}\'"
            else:
                pass
            w.write(f'{info} = {tmp} \n')