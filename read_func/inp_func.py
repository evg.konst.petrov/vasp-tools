#!/usr/bin/env python3
from read_func.func import *

class Inp():
    '''
    A class to read VASP parameters from INCAR and OUTCAR
    '''
    # def get_INCAR(self, filename='INCAR'):
    #     INCAR = load(filename)
    #     return(INCAR)
    def get_Nb(self):
        cond1 = self.LNONCOLLINEAR == 'T' or self.LSORBIT == 'T' or self.ISPIN == 2
        cond2 = self.LSORBIT == 'F' or self.ISPIN == 1
        Nb = int( find_line_1('number of bands    NBANDS', self.file_head).split()[-1])
        if cond1:
            pass
        elif cond2:
            Nb = Nb 
        print(f'Found {Nb} bands')
        return(Nb)

    def get_efermi(self):
        self.efermi_line = find_index_last('E-fermi', self.file)
        efermi = float(self.file[self.efermi_line].split()[2])
        return(efermi)

    def get_nspin(self):
        if self.LNONCOLLINEAR == 'T' and self.ISPIN == 1:
            nspin = 3
        elif self.LNONCOLLINEAR == 'F' and self.ISPIN == 2:
            nspin = 1
        elif self.LNONCOLLINEAR == 'F' and self.ISPIN == 1:
            nspin = 0
        print(f'Detected nspin = {nspin}')
        return(nspin)

    def get_spin_axes_list(self):
        if self.nspin == 3:
            spin_axes_list = ['x', 'y', 'z']
        elif self.nspin == 1:
            spin_axes_list = ['z']
        else:
            spin_axes_list = []
        return(spin_axes_list)

    def __init__(self, path='.', filename='OUTCAR'):
        self.file = load(f'{path}/{filename}')
        self.file_head = self.file[:find_index_1('Iteration    1(   1)', self.file)]
        self.name = self.file_head[find_index_1('POSCAR', self.file_head)].split()[-1]
        self.ISMEAR = int(get_val('ISMEAR', self.file_head))
        self.ISPIN = int(get_val('ISPIN', self.file_head))
        self.LNONCOLLINEAR = get_val('LNONCOLLINEAR', self.file_head)
        self.LSORBIT = get_val('LSORBIT', self.file_head)
        self.LORBIT = int(get_val('LORBIT', self.file_head))
        self.LHFCALC = get_val('LHFCALC', self.file_head)
        self.METAGGA = get_val('METAGGA', self.file_head)
        self.LMAXMIX = int(get_val('LMAXMIX', self.file_head))
        self.nspin = self.get_nspin()
        self.spin_axes_list = self.get_spin_axes_list()
        self.Ne = int(float(get_val('NELECT', self.file_head)))
        self.Nb = self.get_Nb()
        self.Nk = int(get_val('NKPTS', self.file_head))
        # self.efermi_line = 0
        # self.efermi = self.get_efermi()
