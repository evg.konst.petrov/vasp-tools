read_args_dict = {
    '-b':'bands',
    '--bands': 'bands',
    '-d': 'dos',
    '--dos': 'dos',
    '-phb': 'phonon bands',
    '--phonon_bands': 'phbands'
}

tool_args_dict = {
    '-bz': 'bz'
}

read_key_args_list = ['--efermi', '--no-procar', '--no-sphproj', '--load-full-procar']


possible_read_args = read_args_dict.keys()
possible_tool_args = tool_args_dict.keys()