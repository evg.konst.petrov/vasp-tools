#!/usr/bin/env python3

import numpy as np
import os, sys
os.environ['QT_API'] = 'pyqt5'
os.environ['ETS_TOOLKIT'] = 'qt4'
import mayavi.mlab as mv

if len(sys.argv) > 2:
    filename = sys.argv[2]
    from read_func.poscar_func import Poscar
    structure = Poscar(filename)
    rec_basis = np.linalg.inv(structure.basis.T)
    rec_basis_a = np.linalg.inv(structure.basis_a.T)

    try:
        filename = sys.argv[3]
        with open(filename) as f:
            file = f.read().split('\n')
        kpl = int(file[1])
        is_rec = 'rec' in file[3]
        is_line = 'L' in file[2]
        spec_k = np.loadtxt(file[4:], usecols=(0,1,2))
        # kpoints = np.ndarray((kpl*len(spec_k)/2, 3))
        kpoints = np.array([]).reshape((0,3))
        if is_line:
            print('Reading kpoints in line mode.')
            for i in range(0, len(spec_k), 2):
                new_k = \
                np.array([np.linspace(spec_k[i, 0], spec_k[i+1,0], kpl), 
                        np.linspace(spec_k[i, 1], spec_k[i+1,1], kpl),
                        np.linspace(spec_k[i, 2], spec_k[i+1,2], kpl)]).T
                kpoints = np.vstack((kpoints, new_k))
        # print(kpoints)
        if is_rec:
            kpoints_rec = kpoints
            kpoints_cart = kpoints @ rec_basis
    except:
        print('Cant draw kpoints')
else:
    from read_jobs.read_input import reciprocal
    rec_basis = reciprocal.rec_basis
    rec_basis_a = reciprocal.rec_basis_a

    try:
        kpoints_cart = reciprocal.kpoints @ rec_basis
    except:
        print('Cant draw kpoints')

def run():
    print('Drawing IBZ and kpoints...')
    
    # make rec nodes
    rec_nodes = []
    for x in [0, -1, 1, -2, 2]:
        for y in [0, -1, 1, -2, 2]:
            for z in [0, -1, 1, -2, 2]:
                rec_nodes.append([x,y,z])
    rec_nodes = np.array(rec_nodes) @ rec_basis

    # draw arrows
    rec_vectors = [
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]]
    arr_colors = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]
    for i, v in enumerate(rec_vectors):
        vec = np.array(v) @ rec_basis
        print(vec)
        mv.quiver3d(*[*[0, 0, 0], *vec],
                    resolution=32, scale_factor=1,
                    color=arr_colors[i],
                    line_width=2)

    # make voronoi
    from scipy.spatial import ConvexHull, Voronoi
    vor = Voronoi(rec_nodes)
    vertices = vor.vertices[vor.regions[ vor.point_region[0] ]]
    poly = ConvexHull(vertices)
    sx, sy, sz = vertices[:,0], vertices[:,1], vertices[:,2]
    mv.triangular_mesh(sx, sy, sz, poly.simplices, color=(1,0.0,0.0), opacity=0.1)
    scale_factor = np.linalg.det(rec_basis_a)
    
    mv.points3d( kpoints_cart[:,0], kpoints_cart[:,1], kpoints_cart[:,2], color=(0, 1, 0), scale_mode='none', scale_factor=scale_factor*10 )

        # ridges.append(vertices[simplex])
    # mv.plot3d(r[:,0], r[:,1], r[:,2], tube_radius=None, color=(0,0,0), line_width=scale_factor*10000)
    
    current_poly_vertices = vor.regions[ vor.point_region[0] ]
    ridges = [] 
    # print(vor.ridge_vertices)
    for r in vor.ridge_vertices:
        if [-1] in r:
            continue

        try:
            r.remove(-1)
        except:
            pass
        if all( i in current_poly_vertices for i in r ):
            ridges.append( 
                np.concatenate((
                vor.vertices[r], vor.vertices[r][0][None, :] )
            ))

    for r in ridges:
        mv.plot3d(r[:,0], r[:,1], r[:,2], tube_radius=None, color=(0,0,0), line_width=scale_factor*1000)

    mv.show()